package com.ams.jakewhartongithubreposandroid.roomDB

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ams.jakewhartongithubreposandroid.common.models.GithubRepo
import com.ams.jakewhartongithubreposandroid.common.models.Owner
import com.ams.jakewhartongithubreposandroid.roomDB.roonDAOs.GitHubDAO


@Database(entities = [GithubRepo::class, Owner::class], version = 1, exportSchema = false)
@TypeConverters(DateTypeConverter::class)
abstract class AppDatabase: RoomDatabase() {
 abstract fun getGitHubRepo(): GitHubDAO
}