package com.ams.jakewhartongithubreposandroid.roomDB.roonDAOs

import androidx.paging.DataSource
import androidx.room.*
import com.ams.jakewhartongithubreposandroid.common.models.GithubRepo
import io.reactivex.Completable

@Dao
interface GitHubDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(repos: List<GithubRepo>): Completable
    @Query("SELECT * FROM GithubRepo")
    fun getAll(): DataSource.Factory<Int, GithubRepo>
    @Query("DELETE FROM GithubRepo")
    fun flushTable()
}
