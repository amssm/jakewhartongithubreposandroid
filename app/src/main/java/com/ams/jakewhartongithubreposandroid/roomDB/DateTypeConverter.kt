package com.ams.jakewhartongithubreposandroid.roomDB

import androidx.room.TypeConverter
import com.ams.androiddevkit.baseClasses.networking.GsonUtils
import com.ams.jakewhartongithubreposandroid.common.Constants
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class DateTypeConverter {

    private val gson: Gson by lazy { GsonUtils().getCustomGsonConverter(Constants.DATE_FORMAT.APP) }

    @TypeConverter
    fun fromString(value: String): Date {
        val type = object: TypeToken<Date>() {}.type
        return gson.fromJson(value, type)
    }

    @TypeConverter
    fun fromDate(date: Date): String {
        return gson.toJson(date)
    }
}
