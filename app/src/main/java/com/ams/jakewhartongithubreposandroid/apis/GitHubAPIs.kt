package com.ams.jakewhartongithubreposandroid.apis

import com.ams.jakewhartongithubreposandroid.common.models.GithubRepo
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface GitHubAPIs {
    @GET("users/JakeWharton/repos?per_page=15")
    fun getGitHubReposList(@Query("page") page: Int): Single<List<GithubRepo>>
}
