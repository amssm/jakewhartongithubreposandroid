package com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.ams.jakewhartongithubreposandroid.common.baseClasses.mvvm.AppActivityViewModel
import com.ams.jakewhartongithubreposandroid.common.models.GithubRepo

class GitHubReposListingViewModel(private val gitHubReposListingRepo: GitHubReposListingRepo): AppActivityViewModel<GitHubReposListingActivityStates>() {

    lateinit var gitHuReposLivePagedList: LiveData<PagedList<GithubRepo>>

    override fun onViewCreated(owner: LifecycleOwner) {
        super.onViewCreated(owner)
        gitHuReposLivePagedList = gitHubReposListingRepo.getPagedListBuilder().build()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        gitHubReposListingRepo.clean()
    }
}