package com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene.recyclerView

import android.annotation.SuppressLint
import android.view.View
import androidx.core.content.ContextCompat
import com.ams.androiddevkit.baseClasses.recyclerView.BaseViewHolder
import com.ams.jakewhartongithubreposandroid.R
import com.ams.jakewhartongithubreposandroid.common.Constants
import com.ams.jakewhartongithubreposandroid.common.utils.loadFrom
import com.ams.jakewhartongithubreposandroid.common.models.GithubRepo
import com.ams.jakewhartongithubreposandroid.common.utils.getFormattedDate
import kotlinx.android.synthetic.main.github_repo_row.view.*

class GitHubReposListViewHolder(private val view: View): BaseViewHolder<GithubRepo, GitHubRepoItemListener>(view) {

    @SuppressLint("SetTextI18n")
    public override fun onBind(item: GithubRepo?, listener: GitHubRepoItemListener?) {
        view.repoName.text = item?.name
        view.repoDescription.text = if(item?.description != null) item.description else ""
        view.updatedAtLabelValue.text = item?.updatedAt?.getFormattedDate()
        view.forksCountLabel.text = item?.forksCount
        view.startLabel.text = "⭐   ${item?.starsCount}"
        if (item?.language != null) {
            view.languageLabel.text = item.language
             view.languagesColorView.setBackgroundColor(getColorForLanguage(item.language))
        }
        view.repoState.visibility = if(item!!.archived) View.VISIBLE else View.GONE
        view.avatarImageView.loadFrom(item.owner.image)
    }

    private fun getColorForLanguage(language: String?): Int {
        val context = view.context
        var color = ContextCompat.getColor(context, R.color.black)
        when(language) {
            Constants.LANGUAGES.JAVA_SCRIPT -> color = ContextCompat.getColor(context, R.color.yellow)
            Constants.LANGUAGES.SHELL -> color = ContextCompat.getColor(context, R.color.green)
            Constants.LANGUAGES.JAVA -> color = ContextCompat.getColor(context, R.color.brown)
            Constants.LANGUAGES.KOTLIN -> color = ContextCompat.getColor(context, R.color.orange)
            Constants.LANGUAGES.HTML -> color = ContextCompat.getColor(context, R.color.red)
            Constants.LANGUAGES.PYTHON -> color = ContextCompat.getColor(context, R.color.blue)
            Constants.LANGUAGES.CSS -> color = ContextCompat.getColor(context, R.color.purple)
        }
        return color
    }
}