package com.ams.jakewhartongithubreposandroid.scenes.splashScene

import androidx.lifecycle.LifecycleOwner
import com.ams.jakewhartongithubreposandroid.common.baseClasses.mvvm.AppActivityViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class SplashViewModel: AppActivityViewModel<SplashActivityStates>() {

    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
        disposables.add(Observable
            .timer(3, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                updateViewState(SplashActivityStates.NextScreen)
            })
    }
}