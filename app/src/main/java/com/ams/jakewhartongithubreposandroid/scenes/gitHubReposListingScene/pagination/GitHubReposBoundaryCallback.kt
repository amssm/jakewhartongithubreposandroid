package com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene.pagination

import androidx.paging.PagedList
import com.ams.jakewhartongithubreposandroid.apis.GitHubAPIs
import com.ams.jakewhartongithubreposandroid.common.models.GithubRepo
import com.ams.jakewhartongithubreposandroid.common.utils.PagingRequestHelper
import com.ams.jakewhartongithubreposandroid.roomDB.roonDAOs.GitHubDAO
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors

class GitHubReposBoundaryCallback (private val gitHubDAO: GitHubDAO, private val gitHubAPIs: GitHubAPIs): PagedList.BoundaryCallback<GithubRepo>() {

    private val disposables: CompositeDisposable by lazy { CompositeDisposable() }
    private val executor = Executors.newSingleThreadExecutor()
    private val pagingRequestHelper =
        PagingRequestHelper(executor)
    private var lastRequestedPage = 1


    override fun onZeroItemsLoaded() {
        super.onZeroItemsLoaded()
        executeFetching(PagingRequestHelper.RequestType.INITIAL)
    }

    override fun onItemAtEndLoaded(itemAtEnd: GithubRepo) {
        super.onItemAtEndLoaded(itemAtEnd)
        executeFetching(PagingRequestHelper.RequestType.AFTER)
    }

    private fun fetchReposList(page: Int): Single<List<GithubRepo>> {
        return gitHubAPIs.getGitHubReposList(page).subscribeOn(Schedulers.io())
    }

    private fun insertReposListIntoDatabase(reposList: List<GithubRepo>): Completable {
        return gitHubDAO.insert(reposList).observeOn(AndroidSchedulers.mainThread())
    }

    private fun executeFetching(requestMode: PagingRequestHelper.RequestType) {
        pagingRequestHelper.runIfNotRunning(requestMode) { pagingRequestHelperCallBack ->
            disposables.add(fetchReposList(lastRequestedPage)
                .flatMapCompletable {
                    insertReposListIntoDatabase(it)
                }
                .subscribe({ executor.execute {
                    lastRequestedPage++
                    pagingRequestHelperCallBack.recordSuccess()
                } },
                    { error ->
                        println("GitHubDBBoundaryCallback " + error.stackTrace)
                        pagingRequestHelperCallBack.recordFailure(error)
                    })
            )
        }
    }

    fun clean() {
        disposables.dispose()
    }
}
