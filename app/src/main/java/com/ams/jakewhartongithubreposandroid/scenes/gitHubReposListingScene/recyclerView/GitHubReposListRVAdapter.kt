package com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene.recyclerView

import android.view.ViewGroup
import com.ams.jakewhartongithubreposandroid.common.models.GithubRepo
import android.view.LayoutInflater
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.ams.jakewhartongithubreposandroid.R

class GitHubReposListRVAdapter : PagedListAdapter<GithubRepo, GitHubReposListViewHolder>(GitHubComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitHubReposListViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.github_repo_row, parent, false)
        return GitHubReposListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: GitHubReposListViewHolder, position: Int) {
        val gitHubRepo = getItem(position)
        gitHubRepo?.let { holder.onBind(it, null) }
    }


    companion object {
        private val GitHubComparator = object : DiffUtil.ItemCallback<GithubRepo>() {
            override fun areItemsTheSame(oldItem: GithubRepo, newItem: GithubRepo): Boolean =
                oldItem.repoId == newItem.repoId
            override fun areContentsTheSame(oldItem: GithubRepo, newItem: GithubRepo): Boolean =
                newItem == oldItem
        }
    }
}