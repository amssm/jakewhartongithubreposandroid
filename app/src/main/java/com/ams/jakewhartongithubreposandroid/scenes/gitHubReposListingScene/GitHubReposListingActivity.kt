package com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene

import android.app.AlertDialog
import android.os.Bundle
import androidx.lifecycle.Observer
import com.ams.jakewhartongithubreposandroid.common.baseClasses.mvvm.AppActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import com.ams.jakewhartongithubreposandroid.R
import com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene.recyclerView.GitHubReposListRVAdapter
import kotlinx.android.synthetic.main.activity_github_repos_listing.*

class GitHubReposListingActivity: AppActivity<GitHubReposListingViewModel, GitHubReposListingActivityStates>(GitHubReposListingViewModel::class) {

    private var alertDialog: AlertDialog? = null

    override fun getViewId(): Int {
        return R.layout.activity_github_repos_listing
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        title = getString(R.string.jake_wharton_github_repos)
        initRecyclerViewAdapter()
        configureSwipeToRefresh()
    }

    private fun initRecyclerViewAdapter() {
        val gitHubReposAdapter = GitHubReposListRVAdapter()
        githubReposListRecyclerView.itemAnimator = DefaultItemAnimator()
        getViewModel()
            .gitHuReposLivePagedList
            .observe(this, Observer {
                swipeLayout.isRefreshing = false
                if (it.isEmpty()) {
                    showDialog()
                }
                else {
                    if (alertDialog != null && alertDialog!!.isShowing) {
                        alertDialog!!.dismiss()
                    }
                    gitHubReposAdapter.submitList(it)
                }
            })
        githubReposListRecyclerView.adapter = gitHubReposAdapter
    }

    private fun configureSwipeToRefresh() {
        swipeLayout.setOnRefreshListener {
            getViewModel().gitHuReposLivePagedList.value?.dataSource?.invalidate()
        }
        // Configure refreshing colors
        swipeLayout.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )
    }

    private fun getDialogBuilder(): AlertDialog.Builder {
         return AlertDialog.Builder(this)
            .setTitle(resources.getString(R.string.error))
            .setMessage(resources.getString(R.string.empty_dataset_error))
            .setPositiveButton(resources.getString(R.string.ok), /* listener = */ null)
    }

    private fun showDialog() {
        @Suppress("SENSELESS_COMPARISON")
        if (alertDialog == null) {
            alertDialog = getDialogBuilder().create()
        }
        alertDialog!!.show()
    }
}

