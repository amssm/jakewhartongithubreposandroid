package com.ams.jakewhartongithubreposandroid.scenes.splashScene

sealed class SplashActivityStates {
    object NextScreen: SplashActivityStates()
}