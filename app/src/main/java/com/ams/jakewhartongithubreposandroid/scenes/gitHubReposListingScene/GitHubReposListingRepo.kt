package com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene

import com.ams.jakewhartongithubreposandroid.roomDB.roonDAOs.GitHubDAO
import com.ams.jakewhartongithubreposandroid.common.models.GithubRepo
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ams.jakewhartongithubreposandroid.common.Constants
import com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene.pagination.GitHubReposBoundaryCallback


class GitHubReposListingRepo(private val gitHubReposBoundryCallBack: GitHubReposBoundaryCallback, private val gitHubDAO: GitHubDAO) {

    fun getPagedListBuilder(): LivePagedListBuilder<Int, GithubRepo> {
        val livePageListBuilder = LivePagedListBuilder(gitHubDAO.getAll(), getPagedListConfigs())
        livePageListBuilder.setBoundaryCallback(gitHubReposBoundryCallBack)
        return livePageListBuilder
    }

    private fun getPagedListConfigs(): PagedList.Config {
        return PagedList
            .Config.Builder()
            .setPrefetchDistance(Constants.PAGINATION_OFFSET)
            .setPageSize(Constants.PAGINATION_OFFSET)
            .setEnablePlaceholders(false)
            .build()
    }

    fun clean() {
        gitHubReposBoundryCallBack.clean()
    }
}