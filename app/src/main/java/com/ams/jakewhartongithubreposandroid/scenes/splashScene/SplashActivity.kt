package com.ams.jakewhartongithubreposandroid.scenes.splashScene

import com.ams.jakewhartongithubreposandroid.R
import com.ams.jakewhartongithubreposandroid.common.baseClasses.mvvm.AppActivity
import com.ams.jakewhartongithubreposandroid.common.AppNavigator

class SplashActivity: AppActivity<SplashViewModel, SplashActivityStates>(SplashViewModel::class) {

    override fun getViewId(): Int {
        return R.layout.activity_splash
    }

    override fun onViewStateChanged(state: SplashActivityStates?) {
        AppNavigator.openGitHubReposListingActivity()
    }
}
