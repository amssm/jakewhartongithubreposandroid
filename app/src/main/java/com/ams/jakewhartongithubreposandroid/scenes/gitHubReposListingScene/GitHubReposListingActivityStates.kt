package com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene

sealed class GitHubReposListingActivityStates {
    class Error(val errorMessage: String): GitHubReposListingActivityStates()
}