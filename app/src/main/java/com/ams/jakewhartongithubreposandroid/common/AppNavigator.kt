package com.ams.jakewhartongithubreposandroid.common

import android.content.Context
import android.content.Intent
import com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene.GitHubReposListingActivity

object AppNavigator {

    private fun getActivityContext(): Context? { return App.getContext() }

    fun openGitHubReposListingActivity() {
        val intent = Intent(getActivityContext(), GitHubReposListingActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        getActivityContext()?.startActivity(intent)
    }
}