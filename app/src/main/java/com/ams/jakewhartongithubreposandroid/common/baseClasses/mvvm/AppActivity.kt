package com.ams.jakewhartongithubreposandroid.common.baseClasses.mvvm

import com.ams.androiddevkit.baseClasses.designPatterns.mvvm.BaseMVVMActivity
import kotlin.reflect.KClass

abstract class AppActivity<VM: AppActivityViewModel<ViewState>, ViewState>(klass: KClass<VM>): BaseMVVMActivity<VM, ViewState>(clazz = klass) {}