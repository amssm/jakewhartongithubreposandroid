package com.ams.jakewhartongithubreposandroid.common.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Owner(@PrimaryKey var ownerID: Int,
                 @SerializedName("avatar_url") var image: String)