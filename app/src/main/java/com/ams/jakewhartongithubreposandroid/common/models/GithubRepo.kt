package com.ams.jakewhartongithubreposandroid.common.models

import androidx.room.*
import com.ams.jakewhartongithubreposandroid.roomDB.DateTypeConverter
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class GithubRepo(@PrimaryKey(autoGenerate = false)
                      @ColumnInfo(name = "id")
                      @SerializedName("id")
                      var repoId: Long,
                      var name: String,
                      var description: String?,
                      @TypeConverters(DateTypeConverter::class)
                      val updatedAt: Date,
                      @SerializedName("stargazers_count")
                      var starsCount: Int,
                      var language: String?,
                      var forksCount: String,
                      var archived: Boolean,
                      @Embedded
                      var owner: Owner
)
