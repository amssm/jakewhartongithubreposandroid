package com.ams.jakewhartongithubreposandroid.common.baseClasses.mvvm

import androidx.lifecycle.LifecycleOwner
import com.ams.androiddevkit.baseClasses.designPatterns.mvvm.BaseViewModel
import io.reactivex.disposables.CompositeDisposable

open class AppActivityViewModel<ViewState>: BaseViewModel<ViewState>() {

    protected val disposables: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        disposables.dispose()
    }
}