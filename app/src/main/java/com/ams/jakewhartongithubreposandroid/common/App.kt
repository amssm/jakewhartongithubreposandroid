package com.ams.jakewhartongithubreposandroid.common

import android.app.Application
import android.content.Context
import com.ams.jakewhartongithubreposandroid.common.koinDI.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.Module

@Suppress("unused")
class App: Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        injectKoin()
    }

    private fun injectKoin() {
        // Start koin with the modules list
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(getKoinModules())
        }
    }

    private fun getKoinModules(): List<Module> {
        return listOf(viewModelModules, repositoryModules, restAPIsModules, roomDBModules, paginationModules)
    }

    companion object {

        private lateinit var instance: App
        
        fun getContext(): Context? { return instance.applicationContext }
    }
}
