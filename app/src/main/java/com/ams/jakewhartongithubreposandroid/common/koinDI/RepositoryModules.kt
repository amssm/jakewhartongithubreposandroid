package com.ams.jakewhartongithubreposandroid.common.koinDI

import com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene.GitHubReposListingRepo
import org.koin.core.module.Module
import org.koin.dsl.module

val repositoryModules: Module = module {
    factory { GitHubReposListingRepo(get(), get()) }
}