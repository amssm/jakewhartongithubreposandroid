package com.ams.jakewhartongithubreposandroid.common.koinDI

import androidx.room.Room
import com.ams.jakewhartongithubreposandroid.common.Constants
import com.ams.jakewhartongithubreposandroid.roomDB.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

val roomDBModules: Module = module {
   single {
      Room
         .databaseBuilder(androidContext(), AppDatabase::class.java, Constants.DB_NAME)
         .fallbackToDestructiveMigration()
         .build() }
   factory { get<AppDatabase>().getGitHubRepo() }
}

