package com.ams.jakewhartongithubreposandroid.common.koinDI

import com.ams.jakewhartongithubreposandroid.apis.GitHubAPIs
import com.ams.jakewhartongithubreposandroid.common.baseClasses.networking.AppRestClient
import org.koin.core.module.Module
import org.koin.dsl.module

val restAPIsModules: Module = module {
    single { AppRestClient() }
    factory { get<AppRestClient>().getRetrofitClient(GitHubAPIs::class.java) }
}
