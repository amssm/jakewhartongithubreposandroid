package com.ams.jakewhartongithubreposandroid.common.utils

import android.widget.ImageView
import com.ams.jakewhartongithubreposandroid.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

fun ImageView.loadFrom(url: String,
                       placeHolderImage: Int = R.drawable.image_placeholder,
                       errorImage: Int = R.drawable.image_not_available) {
    Glide.with(context)
        .load(url)
        .apply(RequestOptions.circleCropTransform())
        .placeholder(placeHolderImage)
        .error(errorImage)
        .into(this);
}