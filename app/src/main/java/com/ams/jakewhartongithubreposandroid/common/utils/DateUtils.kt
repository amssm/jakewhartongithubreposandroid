package com.ams.jakewhartongithubreposandroid.common.utils

import com.ams.jakewhartongithubreposandroid.roomDB.DateTypeConverter
import java.util.*

fun Date.getFormattedDate(): String?  {
    return DateTypeConverter().fromDate(this).trim('"')
}