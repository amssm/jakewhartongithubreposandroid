package com.ams.jakewhartongithubreposandroid.common.koinDI

import com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene.pagination.GitHubReposBoundaryCallback
import org.koin.core.module.Module
import org.koin.dsl.module

val paginationModules: Module = module {
    factory {
        GitHubReposBoundaryCallback(
            get(),
            get()
        )
    }
}