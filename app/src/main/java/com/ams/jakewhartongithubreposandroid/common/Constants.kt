package com.ams.jakewhartongithubreposandroid.common

@Suppress("MemberVisibilityCanBePrivate")
interface Constants {

    companion object {
        const val DB_NAME = "GitHubReposDB"
        const val PAGINATION_OFFSET = 15
    }

    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    annotation class LANGUAGES {
        companion object {
            const val JAVA = "Java"
            const val KOTLIN = "Kotlin"
            const val JAVA_SCRIPT = "JavaScript"
            const val SHELL = "Shell"
            const val HTML = "HTML"
            const val PYTHON = "Python"
            const val CSS = "CSS"
        }
    }

    @Suppress("ClassName")
    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    annotation class DATE_FORMAT {
        companion object {
            const val APP = "EEEE, dd MMMM YYYY hh:mm a"
        }
    }
}
