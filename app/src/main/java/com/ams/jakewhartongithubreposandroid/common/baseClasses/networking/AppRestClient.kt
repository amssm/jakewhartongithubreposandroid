package com.ams.jakewhartongithubreposandroid.common.baseClasses.networking

import com.ams.androiddevkit.baseClasses.networking.BaseRetrofitClient
import com.ams.androiddevkit.baseClasses.networking.GsonUtils
import com.ams.jakewhartongithubreposandroid.BuildConfig
import com.ams.jakewhartongithubreposandroid.common.Constants
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory


open class AppRestClient: BaseRetrofitClient() {

    init { buildRetrofit() }

    override fun getBaseURL(): String {
        return BuildConfig.BASE_URL
    }

    override fun getConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create(GsonUtils().customGsonConverter)
    }
}
