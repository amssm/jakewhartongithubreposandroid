package com.ams.jakewhartongithubreposandroid.common.koinDI

import com.ams.jakewhartongithubreposandroid.scenes.gitHubReposListingScene.GitHubReposListingViewModel
import com.ams.jakewhartongithubreposandroid.scenes.splashScene.SplashViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModelModules: Module = module {
    viewModel { SplashViewModel() }
    viewModel { GitHubReposListingViewModel(get()) }
}
