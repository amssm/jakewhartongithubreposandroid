## JakeWhartonGitHubReposAndroid
1. Please check out the Master branch which has the latest changes.
2. I have used a library called [Android Base (Still in development)](https://github.com/ahmadmssm/AndroidBase) which i built to facilitate some repeatable tasks, It is a wrapper on top of other libraries like Retrofit and Gson.
3. I used Koin to apply dependency injection, Retrofit as a rest client and used some features of the Android architecture components like MVVM, Live data, Room and Paging components as it is more cleaner and it handles some use cases out of the box like pagination instead of handling it by my self.
4. The app loads GitHib repos from remote and caches it to local database and when the user goes offline, he can still able to load the repos from local database, and pagination is handled using Paging component.
5.  If the user opens the app for the first time while having no internet connection, he will get an error message to check his internet connection then refresh to reload data.
5. Also used MVVM architecture.
6. App reuns on Android  22 and above.
7. I had many things to deliver this week, that i barely completed the task in time but i did not have the time to include unit testing so sorry for that.